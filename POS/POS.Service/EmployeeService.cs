﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class EmployeeService
    {
        private readonly AppDbContext _context;


        private EmployeeModel EntityToModel(EmployeeEntity entity)
        {
            EmployeeModel result = new EmployeeModel();

            result.Id = entity.Id;
            result.LastName = entity.LastName;
            result.FirstName = entity.FirstName;
            result.Title = entity.Title;
            result.TitleOfCourtesy = entity.TitleOfCourtesy;
            result.BirthDate = entity.BirthDate;
            result.HireDate = entity.HireDate;
            result.Address = entity.Address;
            result.City = entity.City;
            result.region = entity.region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.Extension = entity.Extension;
            result.HomePhone = entity.HomePhone;
            result.Notes = entity.Notes;
            result.ReportTo = entity.ReportTo;

            return result;
        }

        public void ModelToEntity(EmployeeModel model, EmployeeEntity entity)
        {

            entity.LastName = model.LastName;
            entity.FirstName = model.FirstName;
            entity.Title = model.Title;
            entity.TitleOfCourtesy = model.TitleOfCourtesy;
            entity.BirthDate = model.BirthDate;
            entity.HireDate = model.HireDate;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.region = model.region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Extension = model.Extension;
            entity.HomePhone = model.HomePhone;
            entity.Notes = model.Notes;
            entity.ReportTo = model.ReportTo;
        }

        public EmployeeService(AppDbContext context)
        {
            _context = context;
        }

        public List<EmployeeEntity> GetEmployee()
        {
            return _context.EmployeeEntities.ToList();
        }

        public EmployeeEntity DetailEmployee(int? id)
        {
            return _context.EmployeeEntities.Find(id);
        }

        public void AddEmployee(EmployeeEntity newEmployee)
        {
            _context.EmployeeEntities.Add(newEmployee);
            _context.SaveChanges();
        }

        public void UpdateEmployee(EmployeeModel employee)
        {
            var entity = _context.EmployeeEntities.Find(employee.Id);
            ModelToEntity(employee, entity);
            _context.EmployeeEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteEmployee(int? id)
        {
            var employee = DetailEmployee(id);
            _context.EmployeeEntities.Remove(employee);
            _context.SaveChanges();
        }


    }
}

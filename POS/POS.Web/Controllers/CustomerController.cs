﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class CustomerController : Controller
    {
        private readonly CustomerService _service;

        public CustomerController(AppDbContext context)
        {
            _service = new CustomerService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetCustomer();
            return View(data);
        }

        [HttpGet]
        public  IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save(CustomerModel request)
        {
            if (ModelState.IsValid)
            {
                _service.AddCustomer(new CustomerEntity(request));
                return Redirect("/Customer");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var data = _service.DetailCustomer(id);
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(CustomerModel customer)
        {
            if (ModelState.IsValid)
            {
                _service.UpdateCustomer(customer);
                return Redirect("/Customer");
            }
            return View("Edit", customer);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.DeleteCustomer(id);
            return Redirect("/Customer");
        }
    }
}

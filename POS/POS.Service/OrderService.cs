﻿using POS.Repository;
using POS.ViewModel;
using POS.ViewModel.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class OrderService
    {
        private readonly AppDbContext _context;

        public OrderService(AppDbContext context)
        {
            _context = context;
        }

        private OrderDetailModel EntityToModelDetail(OrderDetailEntity entity)
        {
            OrderDetailModel result = new OrderDetailModel();
            result.Id = entity.Id;
            result.OrderId = entity.OrderId;
            result.ProductId = entity.ProductId;
            result.Price = entity.Price;
            result.Quantity = entity.Quantity;
            result.Discount = entity.Discount;

            return result;
        }

        private OrderDetailEntity ModelToEntityDetail(OrderDetailModel model)
        {
            var entity = new OrderDetailEntity();
            entity.OrderId = model.OrderId;
            entity.ProductId = model.ProductId;
            entity.Price = model.Price;
            entity.Quantity = model.Quantity;
            entity.Discount = model.Discount;

            return entity;
        }


        private OrderModel EntityToModel(OrderEntity entity)
        {
            OrderModel result = new OrderModel();
            result.Id = entity.Id;
            result.CustomerId = entity.CustomerId;
            result.EmployeeId = entity.EmployeeId;
            result.OrderDate = entity.OrderDate;
            result.RequiredDate = entity.RequiredDate;
            result.ShippedDate = entity.ShippedDate;
            result.ShipVia = entity.ShipVia;
            result.Freight = entity.Freight;
            result.ShipName = entity.ShipName;
            result.ShipAddress = entity.ShipAddress;
            result.ShipCity = entity.ShipCity;
            result.ShipRegion = entity.ShipRegion;
            result.ShipPostal = entity.ShipPostal;
            result.ShipCountry = entity.ShipCountry;
            result.OrderDetails = new List<OrderDetailModel>();
            foreach (var item in entity.OrderDetails)
            {
                result.OrderDetails.Add(EntityToModelDetail(item));
            }


            return result;
        }

        private OrderEntity ModelToEntity(OrderModel model)
        {
            var entity = new OrderEntity();
            entity.CustomerId = model.CustomerId;
            entity.EmployeeId = model.EmployeeId;
            entity.OrderDate = model.OrderDate;
            entity.RequiredDate = model.RequiredDate;
            entity.ShippedDate = model.ShippedDate;
            entity.ShipVia = model.ShipVia;
            entity.Freight = model.Freight;
            entity.ShipName = model.ShipName;
            entity.ShipAddress = model.ShipAddress;
            entity.ShipCity = model.ShipCity;
            entity.ShipRegion = model.ShipRegion;
            entity.ShipPostal = model.ShipPostal;
            entity.ShipCountry = model.ShipCountry;
            entity.OrderDetails = new List<OrderDetailEntity>();
            foreach (var item in model.OrderDetails)
            {
                entity.OrderDetails.Add(ModelToEntityDetail(item));  
            }
            return entity;
        }

        private DetailOfOrderResponse EntityToModelResponseDetail(OrderEntity entity)
        {
            var customer = _context.CustomerEntities.Find(entity.CustomerId);
            var response = new DetailOfOrderResponse();
            response.CustomerId = customer.Id;
            response.CustomerName = customer.ContactName;
            response.OrderDate = entity.OrderDate;
            response.RequiredDate = entity.RequiredDate;
            response.ShippedDate = entity.ShippedDate;
            response.ShipVia = entity.ShipVia;
            response.Freight = entity.Freight;
            response.ShipName = entity.ShipName;
            response.ShipAddress = entity.ShipAddress;
            response.ShipCity = entity.ShipCity;
            response.ShipRegion = entity.ShipRegion;
            response.ShipPostal = entity.ShipPostal;
            response.ShipCountry = entity.ShipCountry;
            response.Details = new List<OrderDetailResponse>();

            foreach (var item in entity.OrderDetails)
            {
                response.Details.Add(EntityToModelDetailResponse(item));
            }
            var subtotal = 0.0;
            foreach (var item in response.Details)
            {
                item.Subtotal = item.Quantity * item.Price * (1 - item.Discount / 100);
                subtotal += item.Subtotal;
            }
            response.Subtotal = subtotal;
            response.Tax = 0.1 * subtotal;
            response.Shipping = 0;
            response.Total = response.Subtotal + response.Tax + response.Shipping;

            return response;
        }

        private OrderDetailResponse EntityToModelDetailResponse(OrderDetailEntity entity)
        {
            var model = new OrderDetailResponse();
            var product = _context.ProductEntities.Find(entity.ProductId);

            model.Id = entity.Id;
            model.ProductId = product.Id;
            model.ProductName = product.Name;
            model.Price = entity.Price;
            model.Quantity = entity.Quantity;
            model.Discount = entity.Discount;
            return model;
        }

      

        public List<OrderEntity> GetOrders()
        {
            return _context.OrderEntities.ToList();
        }

        public OrderModel DetailOrder(int? id)
        {
            var order = _context.OrderEntities.Find(id);
            var detail = _context.OrderDetailEntities.Where(x => x.OrderId == id);
            foreach (var item in detail) { }
            return EntityToModel(order);
        }

        public DetailOfOrderResponse DetailInvoice(int? id)
        {
            var orderEntity = _context.OrderEntities.Find(id);
            var orderDetail = _context.OrderDetailEntities.Where(x => x.OrderId == id).ToList();
            orderEntity.OrderDetails = orderDetail;
            var orderResponse = EntityToModelResponseDetail(orderEntity);
            return orderResponse;
        }

        public void AddOrder(OrderModel newOrder)
        {
            var newData = ModelToEntity(newOrder);
            _context.OrderEntities.Add(newData);
            foreach(var item in newData.OrderDetails)
            {
                item.OrderId = newOrder.Id;
                _context.OrderDetailEntities.Add(item);
;            }
            _context.SaveChanges();
        }

        public void UpdateOrder(OrderModel order)
        {
            var entity = _context.OrderEntities.Find(order.Id);
            var orderDetailList = _context.OrderDetailEntities.Where(x => x.OrderId == order.Id).ToList();

            var updatedEntity = ModelToEntity(order);

            entity.CustomerId = updatedEntity.CustomerId;
            entity.EmployeeId = updatedEntity.EmployeeId;
            entity.OrderDate = updatedEntity.OrderDate;
            entity.RequiredDate = updatedEntity.RequiredDate;
            entity.ShippedDate = updatedEntity.ShippedDate;
            entity.ShipVia = updatedEntity.ShipVia;
            entity.Freight = updatedEntity.Freight;
            entity.ShipName = updatedEntity.ShipName;
            entity.ShipAddress = updatedEntity.ShipAddress;
            entity.ShipCity = updatedEntity.ShipCity;
            entity.ShipRegion = updatedEntity.ShipRegion;
            entity.ShipPostal = updatedEntity.ShipPostal;
            entity.ShipCountry = updatedEntity.ShipCountry;
            entity.OrderDetails = updatedEntity.OrderDetails;

           
            _context.OrderEntities.Update(entity);

            foreach (var newItem in entity.OrderDetails)
            {
                newItem.OrderId = order.Id;
                foreach (var item in orderDetailList)
                {
                    if (newItem.ProductId == item.ProductId)
                    {
                        item.ProductId = newItem.ProductId;
                        item.Price = newItem.Price;
                        item.Quantity = newItem.Quantity;
                        item.Discount = newItem.Discount;
                        _context.OrderDetailEntities.Update(item);
                    }
                }

            }


            _context.SaveChanges();
        }

        public void DeleteOrder(int? id)
        {
            var order = _context.OrderEntities.Find(id);
            _context.OrderEntities.Remove(order);

            var detail = _context.OrderDetailEntities.Where(_x => _x.Id == id);
            foreach (var item in detail)
            {
                _context.OrderDetailEntities.Remove(item);
            }


            _context.SaveChanges();
        }

    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace POS.ViewModel
{
    public class CategoryModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
    }
}
﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class CustomerService
    {
        private readonly AppDbContext _context;

        private CustomerModel EntityToModel(CustomerEntity entity)
        {
            CustomerModel result = new CustomerModel();
            result.Id = entity.Id;
            result.CompanyName = entity.CompanyName;
            result.ContactName = entity.ContactName;
            result.ContactTitle = entity.ContactTitle;
            result.Address = entity.Address;
            result.City = entity.City;
            result.region = entity.region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.Phone = entity.Phone;
            result.Fax = entity.Fax;

            return result;
        }

        public void ModelToEntity(CustomerModel model, CustomerEntity entity)
        {
            entity.CompanyName = model.CompanyName;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.region = model.region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
        }

        public CustomerService(AppDbContext context)
        {
            _context = context;
        }

        public List<CustomerEntity> GetCustomer()
        {
            return _context.CustomerEntities.ToList();
        }

        public CustomerEntity DetailCustomer(int? id)
        {
            return _context.CustomerEntities.Find(id);
        }

        public void AddCustomer(CustomerEntity newCustomer)
        {
            _context.CustomerEntities.Add(newCustomer);
            _context.SaveChanges();
        }

        public void UpdateCustomer(CustomerModel customer)
        {
            var entity = _context.CustomerEntities.Find(customer.Id);
            ModelToEntity(customer, entity);
            _context.CustomerEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteCustomer(int? id)
        {
            var customer = DetailCustomer(id);
            _context.CustomerEntities.Remove(customer);
            _context.SaveChanges();
        }
    }
}

﻿using POS.Repository;
using POS.ViewModel;
using System.Runtime.InteropServices;

namespace POS.Service
{
    public class CategoryService
    {
        private readonly AppDbContext _context;
        private CategoryModel EntityToModel(CategoryEntity entity)
        {
            CategoryModel result= new CategoryModel();
            result.Id = entity.Id;
            result.Name = entity.Name;
            result.Description = entity.Description;

            return result;
        }

        public void ModelToEntity(CategoryModel model, CategoryEntity entity)
        {
            entity.Name = model.Name;
            entity.Description = model.Description;
        }

        public CategoryService(AppDbContext context)
        {
            _context = context;
        }

        public List<CategoryEntity> GetCategories()
        {
            return _context.CategoryEntities.ToList();
        }

        public CategoryModel DetailCategory(int? id)
        {
            var category = _context.CategoryEntities.Find(id);
            return EntityToModel(category);
        }
        public void AddThenSaveCategory(CategoryEntity newcategory)
        {
            _context.CategoryEntities.Add(newcategory);
            _context.SaveChanges();
        }

        public void UpdateCategory(CategoryModel category)
        {
           var entity = _context.CategoryEntities.Find(category.Id);
            ModelToEntity(category, entity);
            _context.CategoryEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteCategory(int? id)
        {
            var category = _context.CategoryEntities.Find(id);
            _context.CategoryEntities.Remove(category);
          
            _context.SaveChanges();
        }
    }
}
﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.ViewModel;

namespace POS.Repository
{
    [Table("tbl_categories")]
    public class CategoryEntity
    {
        [Key]
        [Column("category_id")]
        public int Id { get; set; }
        [Required]
        [Column("category_name")]
        public string Name { get; set; }
        [Required]
        [Column("category_description")]
        public string Description { get; set; }

        public ICollection<ProductEntity> Product { get; set; }

        public CategoryEntity()
        {
        }

        public CategoryEntity(CategoryModel vmodel)
        {
            Name = vmodel.Name;
            Description = vmodel.Description;
        }
    }
}

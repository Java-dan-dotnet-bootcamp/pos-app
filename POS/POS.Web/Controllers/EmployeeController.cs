﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.ViewModel;
using POS.Service;

namespace POS.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly EmployeeService _service;
        public EmployeeController(AppDbContext context) 
        {
            _service = new EmployeeService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetEmployee();
            return View(data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }
        [HttpGet]
        public IActionResult AddModal()
        {
            return View("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save(EmployeeModel request) 
        {
            if (ModelState.IsValid)
            {
                _service.AddEmployee(new EmployeeEntity(request));
                return Redirect("/Employee");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var data = _service.DetailEmployee(id);
            return View(data);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(EmployeeModel employee)
        {
            if (ModelState.IsValid)
            {
                _service.UpdateEmployee(employee);
                return Redirect("/Employee");

            }
            return View("Edit", employee);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.DeleteEmployee(id);
            return Redirect("/Employee");
        }

    }
}

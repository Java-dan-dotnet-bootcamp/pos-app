﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace POS.Repository.Migrations
{
    /// <inheritdoc />
    public partial class MigrationBaru : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tbl_categories",
                columns: table => new
                {
                    categoryid = table.Column<int>(name: "category_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    categoryname = table.Column<string>(name: "category_name", type: "text", nullable: false),
                    categorydescription = table.Column<string>(name: "category_description", type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_categories", x => x.categoryid);
                });

            migrationBuilder.CreateTable(
                name: "tbl_customers",
                columns: table => new
                {
                    customerid = table.Column<int>(name: "customer_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    companyname = table.Column<string>(name: "company_name", type: "text", nullable: false),
                    contactname = table.Column<string>(name: "contact_name", type: "text", nullable: false),
                    contacttitle = table.Column<string>(name: "contact_title", type: "text", nullable: false),
                    address = table.Column<string>(type: "text", nullable: false),
                    city = table.Column<string>(type: "text", nullable: false),
                    region = table.Column<string>(type: "text", nullable: false),
                    postalcode = table.Column<string>(name: "postal_code", type: "text", nullable: false),
                    country = table.Column<string>(type: "text", nullable: false),
                    phone = table.Column<string>(type: "text", nullable: false),
                    fax = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_customers", x => x.customerid);
                });

            migrationBuilder.CreateTable(
                name: "tbl_employees",
                columns: table => new
                {
                    employeeid = table.Column<int>(name: "employee_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    lastname = table.Column<string>(name: "last_name", type: "text", nullable: false),
                    firstname = table.Column<string>(name: "first_name", type: "text", nullable: false),
                    title = table.Column<string>(type: "text", nullable: false),
                    titleofcourtsey = table.Column<string>(name: "title_of_courtsey", type: "text", nullable: false),
                    birthdate = table.Column<DateOnly>(name: "birth_date", type: "date", nullable: false),
                    hiredate = table.Column<DateOnly>(name: "hire_date", type: "date", nullable: false),
                    address = table.Column<string>(type: "text", nullable: false),
                    city = table.Column<string>(type: "text", nullable: false),
                    region = table.Column<string>(type: "text", nullable: false),
                    postalcode = table.Column<string>(name: "postal_code", type: "text", nullable: false),
                    country = table.Column<string>(type: "text", nullable: false),
                    homephone = table.Column<string>(name: "home_phone", type: "text", nullable: false),
                    extension = table.Column<string>(type: "text", nullable: false),
                    notes = table.Column<string>(type: "text", nullable: false),
                    reportto = table.Column<string>(name: "report_to", type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_employees", x => x.employeeid);
                });

            migrationBuilder.CreateTable(
                name: "tbl_suppliers",
                columns: table => new
                {
                    supplierid = table.Column<int>(name: "supplier_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    companyname = table.Column<string>(name: "company_name", type: "text", nullable: false),
                    contactname = table.Column<string>(name: "contact_name", type: "text", nullable: false),
                    contacttitle = table.Column<string>(name: "contact_title", type: "text", nullable: false),
                    address = table.Column<string>(type: "text", nullable: false),
                    city = table.Column<string>(type: "text", nullable: false),
                    region = table.Column<string>(type: "text", nullable: false),
                    postalcode = table.Column<string>(name: "postal_code", type: "text", nullable: false),
                    country = table.Column<string>(type: "text", nullable: false),
                    phone = table.Column<string>(type: "text", nullable: false),
                    fax = table.Column<string>(type: "text", nullable: false),
                    homepage = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_suppliers", x => x.supplierid);
                });

            migrationBuilder.CreateTable(
                name: "tbl_orders",
                columns: table => new
                {
                    orderid = table.Column<int>(name: "order_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    customerid = table.Column<int>(name: "customer_id", type: "integer", nullable: false),
                    employeeid = table.Column<int>(name: "employee_id", type: "integer", nullable: false),
                    orderdate = table.Column<DateTime>(name: "order_date", type: "timestamp without time zone", nullable: false),
                    requireddate = table.Column<DateTime>(name: "required_date", type: "timestamp without time zone", nullable: false),
                    shippeddate = table.Column<DateTime>(name: "shipped_date", type: "timestamp without time zone", nullable: false),
                    shipvia = table.Column<string>(name: "ship_via", type: "text", nullable: false),
                    freight = table.Column<int>(type: "integer", nullable: false),
                    shipname = table.Column<string>(name: "ship_name", type: "text", nullable: false),
                    shipaddress = table.Column<string>(name: "ship_address", type: "text", nullable: false),
                    shipcity = table.Column<string>(name: "ship_city", type: "text", nullable: false),
                    shipregion = table.Column<string>(name: "ship_region", type: "text", nullable: false),
                    shippostalcode = table.Column<string>(name: "ship_postal_code", type: "text", nullable: false),
                    shipcountry = table.Column<string>(name: "ship_country", type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_orders", x => x.orderid);
                    table.ForeignKey(
                        name: "FK_tbl_orders_tbl_customers_customer_id",
                        column: x => x.customerid,
                        principalTable: "tbl_customers",
                        principalColumn: "customer_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tbl_orders_tbl_employees_employee_id",
                        column: x => x.employeeid,
                        principalTable: "tbl_employees",
                        principalColumn: "employee_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tbl_products",
                columns: table => new
                {
                    productid = table.Column<int>(name: "product_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    productname = table.Column<string>(name: "product_name", type: "text", nullable: false),
                    supplierid = table.Column<int>(name: "supplier_id", type: "integer", nullable: false),
                    categoryid = table.Column<int>(name: "category_id", type: "integer", nullable: false),
                    quantityperunit = table.Column<int>(name: "quantity_per_unit", type: "integer", nullable: false),
                    unitprice = table.Column<double>(name: "unit_price", type: "double precision", nullable: false),
                    unitinstock = table.Column<int>(name: "unit_in_stock", type: "integer", nullable: false),
                    unitonorder = table.Column<int>(name: "unit_on_order", type: "integer", nullable: false),
                    reorderlevel = table.Column<int>(name: "reorder_level", type: "integer", nullable: false),
                    discontinued = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_products", x => x.productid);
                    table.ForeignKey(
                        name: "FK_tbl_products_tbl_categories_category_id",
                        column: x => x.categoryid,
                        principalTable: "tbl_categories",
                        principalColumn: "category_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tbl_products_tbl_suppliers_supplier_id",
                        column: x => x.supplierid,
                        principalTable: "tbl_suppliers",
                        principalColumn: "supplier_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "tbl_order_details",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    orderid = table.Column<int>(name: "order_id", type: "integer", nullable: false),
                    productid = table.Column<int>(name: "product_id", type: "integer", nullable: false),
                    unitprice = table.Column<double>(name: "unit_price", type: "double precision", nullable: false),
                    quantity = table.Column<int>(type: "integer", nullable: false),
                    discount = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_order_details", x => x.id);
                    table.ForeignKey(
                        name: "FK_tbl_order_details_tbl_orders_order_id",
                        column: x => x.orderid,
                        principalTable: "tbl_orders",
                        principalColumn: "order_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tbl_order_details_tbl_products_product_id",
                        column: x => x.productid,
                        principalTable: "tbl_products",
                        principalColumn: "product_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tbl_order_details_order_id",
                table: "tbl_order_details",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_order_details_product_id",
                table: "tbl_order_details",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_orders_customer_id",
                table: "tbl_orders",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_orders_employee_id",
                table: "tbl_orders",
                column: "employee_id");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_products_category_id",
                table: "tbl_products",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_products_supplier_id",
                table: "tbl_products",
                column: "supplier_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbl_order_details");

            migrationBuilder.DropTable(
                name: "tbl_orders");

            migrationBuilder.DropTable(
                name: "tbl_products");

            migrationBuilder.DropTable(
                name: "tbl_customers");

            migrationBuilder.DropTable(
                name: "tbl_employees");

            migrationBuilder.DropTable(
                name: "tbl_categories");

            migrationBuilder.DropTable(
                name: "tbl_suppliers");
        }
    }
}

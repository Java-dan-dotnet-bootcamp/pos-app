﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class ProductService
    {
        private readonly AppDbContext _context;

        private ProductModel EntityToModel(ProductEntity entity)
        {
            ProductModel result = new ProductModel();
            result.Id = entity.Id;
            result.Name = entity.Name;
            result.SupplierId = entity.SupplierId;

            result.CategoryID = entity.CategoryID;

            result.Quantity = entity.Quantity;
            result.Price = entity.Price;
            result.Stock = entity.Stock;
            result.Order = entity.Order;
            result.ReorderLevel = entity.ReorderLevel;
            result.Discontinued = entity.Discontinued;

            return result;
        }

        public void ModelToEntity(ProductModel model, ProductEntity entity)
        {
            entity.Name = model.Name;
            entity.SupplierId = model.SupplierId;
            entity.CategoryID = model.CategoryID;
            entity.Quantity = model.Quantity;
            entity.Price = model.Price;
            entity.Stock = model.Stock;
            entity.Order = model.Order;
            entity.ReorderLevel = model.ReorderLevel;
            entity.Discontinued = model.Discontinued;
        }

        public ProductService(AppDbContext context)
        {
            _context = context;
        }

        public List<ProductEntity> GetProducts()
        {
            return _context.ProductEntities.ToList();
        }

        public ProductModel DetailProduct(int? id) 
        {
            var product = _context.ProductEntities.Find(id);
            return EntityToModel(product);
        }

        public void AddProduct(ProductEntity newProduct)
        {
            _context.ProductEntities.Add(newProduct);
            _context.SaveChanges();
        }

        public void UpdateProduct(ProductModel product)
        {
            var entity = _context.ProductEntities.Find(product.Id);
            ModelToEntity(product, entity);
            _context.ProductEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteProduct(int? id)
        {
            var product = _context.ProductEntities.Find(id);
            _context.ProductEntities.Remove(product);
            _context.SaveChanges();
        }

    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly OrderService _service;
        private readonly CustomerService _serviceCus;
        private readonly EmployeeService _serviceEmp;
        private readonly ProductService _servicePrd;

        public OrderController(AppDbContext context) 
        {
            _service = new OrderService(context);
            _serviceCus = new CustomerService(context);
            _serviceEmp = new EmployeeService(context);           
            _servicePrd = new ProductService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetOrders();
            return View(data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.Customer = new SelectList(_serviceCus.GetCustomer(), "Id", "ContactName");
            ViewBag.Employee = new SelectList(_serviceEmp.GetEmployee(), "Id", "FirstName");
            ViewBag.Product = new SelectList(_servicePrd.GetProducts(), "Id", "Name");
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            ViewBag.Customer = new SelectList(_serviceCus.GetCustomer(), "Id", "ContactName");
            ViewBag.Employee = new SelectList(_serviceEmp.GetEmployee(), "Id", "FirstName");
            ViewBag.Product = new SelectList(_servicePrd.GetProducts(), "Id", "Name");
            return View("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry, OrderDetails")] OrderModel request)
        {
            if (ModelState.IsValid)
            {
                _service.AddOrder(request);
                return Redirect("Index");
            }
            return View("Add", request);
        }


        [HttpGet]
        public IActionResult Details(int? id)
        {
            var data = _service.DetailOrder(id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Invoice(int? id)
        {
            var data = _service.DetailInvoice(id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            ViewBag.Customer = new SelectList(_serviceCus.GetCustomer(), "Id", "ContactName");
            ViewBag.Employee = new SelectList(_serviceEmp.GetEmployee(), "Id", "FirstName");
            var data = _service.DetailOrder(id);
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry, OrderDetails")] OrderModel order)
        {
            if( ModelState.IsValid)
            {
                _service.UpdateOrder(order);
                return Redirect("/Order");
            }
            return View("Edit", order);
        }


        [HttpGet]

        public IActionResult Delete(int? id)
        {
            _service.DeleteOrder(id);
            return Redirect("/Order");
        }


    }
}

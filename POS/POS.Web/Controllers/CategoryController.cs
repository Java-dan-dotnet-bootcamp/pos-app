﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class CategoryController : Controller
    {

        private readonly CategoryService _service;

        public CategoryController(AppDbContext context)
        {
            _service = new CategoryService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetCategories();
            return View(data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("Name, Description")] CategoryModel request)
        {
            if (ModelState.IsValid)
            {
                _service.AddThenSaveCategory(new CategoryEntity(request));
                return Redirect("/Category");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var data = _service.DetailCategory(id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var data = _service.DetailCategory(id);
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("Id, Name, Description")]CategoryModel category)
        {
            if (ModelState.IsValid)
            {
               
                _service.UpdateCategory(category);
                return Redirect("/Category");
            }
            return View("Edit", category);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.DeleteCategory(id);
            return Redirect("/Category");
        }

    }
}

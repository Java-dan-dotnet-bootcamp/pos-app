﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class OrderDetailService
    {
        private readonly AppDbContext _context;


        public OrderDetailModel EntityToModel(OrderDetailEntity entity)
        {
            OrderDetailModel result = new OrderDetailModel();
            result.Id = entity.Id;
            result.OrderId = entity.OrderId;
            result.ProductId = entity.ProductId;
            result.Price = entity.Price;
            result.Quantity = entity.Quantity;
            result.Discount = entity.Discount;

            return result;
        }

        public void ModelToEntity(OrderDetailModel model, OrderDetailEntity entity)
        {
            entity.OrderId = model.OrderId;
            entity.ProductId = model.ProductId;
            entity.Price = model.Price;
            entity.Quantity = model.Quantity;
            entity.Discount = model.Discount;
        }

        public OrderDetailService(AppDbContext context) 
        {
            _context = context;
        }

        public List<OrderDetailEntity> GetOrdersDetail()
        {
            return _context.OrderDetailEntities.ToList();
        }

        public OrderDetailModel DetailOrderDetail(int? id)
        {
            var order = _context.OrderDetailEntities.Find(id);
            return EntityToModel(order);
        }

        public void AddOrderDetail(OrderDetailEntity newOrder)
        {
            _context.OrderDetailEntities.Add(newOrder);
            _context.SaveChanges();
        }

        public void UpdateOrderDetail(OrderDetailModel order)
        {
            var entity = _context.OrderDetailEntities.Find(order.Id);
            ModelToEntity(order, entity);
            _context.OrderDetailEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteOrderDetail(int? id)
        {
            var order = _context.OrderDetailEntities.Find(id);
            _context.OrderDetailEntities.Remove(order);
            _context.SaveChanges();
        }


    }
}

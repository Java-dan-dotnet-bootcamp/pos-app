﻿
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_orders")]
    public class OrderEntity
    {
        [Key]
        [Column("order_id")]
        public int Id { get; set; }
        [Column("customer_id")]
        public int CustomerId { get; set; }
        public CustomerEntity Customer { get; set;}

        [Column("employee_id")]
        public int EmployeeId { get; set; }
        public EmployeeEntity Employee { get; set; }

        [Column("order_date")]
        public DateTime OrderDate { get; set; }
        [Column("required_date")]
        public DateTime RequiredDate { get; set; }
        [Column("shipped_date")]
        public DateTime ShippedDate { get; set; }
        [Column("ship_via")]
        public string ShipVia { get; set; }
        [Column("freight")]
        public int Freight { get; set; }
        [Column("ship_name")]
        public string ShipName { get; set; }
        [Column("ship_address")]
        public string ShipAddress { get; set; }
        [Column("ship_city")]
        public string ShipCity { get; set; }
        [Column("ship_region")]
        public string ShipRegion { get; set; }
        [Column("ship_postal_code")]
        public string ShipPostal { get; set; }
        [Column("ship_country")]
        public string ShipCountry { get; set; }

        public List<OrderDetailEntity> OrderDetails { get; set; }


        public OrderEntity()
        {

        }

        public OrderEntity(OrderModel vmodel)
        {
            CustomerId = vmodel.CustomerId;
            EmployeeId = vmodel.EmployeeId;
            OrderDate = vmodel.OrderDate;
            RequiredDate = vmodel.RequiredDate;
            ShippedDate = vmodel.ShippedDate;
            ShipVia = vmodel.ShipVia;
            Freight = vmodel.Freight;
            ShipName = vmodel.ShipName;
            ShipAddress = vmodel.ShipAddress;
            ShipCity = vmodel.ShipCity;
            ShipRegion = vmodel.ShipRegion;
            ShipPostal = vmodel.ShipPostal;
            ShipCountry = vmodel.ShipCountry;

        }

    }
}

﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class SupplierController : Controller
    {
        private readonly SupplierService _service;

        public SupplierController(AppDbContext context)
        {
            _service = new SupplierService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetSuppliers();
            return View(data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        } 
        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        public IActionResult Save(SupplierModel supplier)
        {
            if (ModelState.IsValid)
            {
                _service.AddSupplier(new SupplierEntity(supplier));
                return Redirect("/Supplier");
            }
            return View("Add", supplier);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var data = _service.DetailSupplier(id);
            return View(data);
        }

        [HttpPost]
        public IActionResult Update(SupplierModel supplier)
        {
            _service.UpdateSupplier(supplier);
            return Redirect("/Supplier");
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.DeleteSupplier(id);
            return Redirect("/Supplier");
        }
    }
}

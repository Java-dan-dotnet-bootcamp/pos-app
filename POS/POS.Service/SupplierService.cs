﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Net;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class SupplierService
    {
        private readonly AppDbContext _context;

        private SupplierModel EntityToModel(SupplierEntity entity)
        {
            SupplierModel result = new SupplierModel();
            result.Id = entity.Id;
            result.CompanyName = entity.CompanyName;
            result.ContactName = entity.ContactName;
            result.ContactTitle = entity.ContactTitle;
            result.Address = entity.Address;
            result.City = entity.City;
            result.region = entity.region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.Phone = entity.Phone;
            result.Fax = entity.Fax;
            result.Homepage = entity.Homepage;

            return result;
        }

        public void ModelToEntity(SupplierModel model, SupplierEntity entity)
        {
            entity.CompanyName = model.CompanyName;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.region = model.region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
            entity.Homepage = model.Homepage;
        }

        public SupplierService(AppDbContext context)
        {
            _context = context;
        }
    
        public List<SupplierEntity> GetSuppliers()
        {
            return _context.SupplierEntities.ToList();
        }

        public SupplierEntity DetailSupplier(int? id)
        {
            return _context.SupplierEntities.Find(id);
        }

        public void AddSupplier(SupplierEntity newSupplier)
        {
            _context.SupplierEntities.Add(newSupplier);
            _context.SaveChanges();
        }

        public void UpdateSupplier(SupplierModel supplier)
        {
            var entity = _context.SupplierEntities.Find(supplier.Id);
            ModelToEntity(supplier, entity);
            _context.SupplierEntities.Update(entity);
            _context.SaveChanges();
        }

        public void DeleteSupplier(int? id)
        {
            var supplier = DetailSupplier(id);
            _context.SupplierEntities.Remove(supplier);
            _context.SaveChanges();
        }


    
    }
}

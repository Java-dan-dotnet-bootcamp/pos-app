﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

        }

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=pos_db;Username=postgres;Password=pohon4785");*/

        public DbSet<CategoryEntity> CategoryEntities => Set<CategoryEntity>();
        public DbSet<ProductEntity> ProductEntities => Set<ProductEntity>();
        public DbSet<SupplierEntity> SupplierEntities => Set<SupplierEntity>();
        public DbSet<OrderDetailEntity> OrderDetailEntities => Set<OrderDetailEntity>();
        public DbSet<OrderEntity> OrderEntities => Set<OrderEntity>();
        public DbSet<CustomerEntity> CustomerEntities => Set<CustomerEntity>();
        public DbSet<EmployeeEntity> EmployeeEntities => Set<EmployeeEntity>();




    }
}

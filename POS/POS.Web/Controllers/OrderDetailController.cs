﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class OrderDetailController : Controller
    {
        private readonly OrderDetailService _service;

        public OrderDetailController(AppDbContext context)
        {
            _service = new OrderDetailService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetOrdersDetail();
            return View(data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save(OrderDetailModel request)
        {
            if (ModelState.IsValid)
            {
                _service.AddOrderDetail(new OrderDetailEntity(request));
                return Redirect("/OrderDetail");
            }
            return View("Add", request);
        }


        [HttpGet]
        public IActionResult Details(int? id)
        {
            var data = _service.DetailOrderDetail(id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var data = _service.DetailOrderDetail(id);
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(OrderDetailModel order)
        {
            if (ModelState.IsValid)
            {
                _service.UpdateOrderDetail(order);
                return Redirect("/OrderDetail");
            }
            return View("Edit", order);
        }


        [HttpGet]

        public IActionResult Delete(int? id)
        {
            _service.DeleteOrderDetail(id);
            return Redirect("/OrderDetail");
        }

    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class ProductController : Controller
    {
        public readonly ProductService _service;

        public readonly CategoryService _serviceCtr;
        public readonly SupplierService _serviceSpr;

        public ProductController(AppDbContext context)
        {
            _service = new ProductService(context);
            _serviceCtr = new CategoryService(context);
            _serviceSpr = new SupplierService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetProducts();
            return View(data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.Category = new SelectList(_serviceCtr.GetCategories(), "Id", "Name");
            ViewBag.Supplier = new SelectList(_serviceSpr.GetSuppliers(), "Id", "CompanyName");
            return View();
        } 
        public IActionResult AddModal()
        {
            ViewBag.Category = new SelectList(_serviceCtr.GetCategories(), "Id", "Name");
            ViewBag.Supplier = new SelectList(_serviceSpr.GetSuppliers(), "Id", "CompanyName");
            return PartialView("_Add");
        }

        [HttpPost]
        public IActionResult Save(ProductModel product)
        {
            if (ModelState.IsValid)
            {
                _service.AddProduct(new ProductEntity(product));
                return Redirect("/Product");
            }
            return View("Add", product);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var data = _service.DetailProduct(id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            ViewBag.Category = new SelectList(_serviceCtr.GetCategories(), "Id", "Name");
            ViewBag.Supplier = new SelectList(_serviceSpr.GetSuppliers(), "Id", "CompanyName");
            var data = _service.DetailProduct(id);
            return View(data);
        }

        [HttpPost]
        public IActionResult Update(ProductModel product)
        {
            if (ModelState.IsValid)
            {
             
                _service.UpdateProduct(product);
                return Redirect("/Product");
            }
            return View("Edit", product);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.DeleteProduct(id);
            return Redirect("/Product");
        }
    }
}

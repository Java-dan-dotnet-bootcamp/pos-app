﻿using POS.Repository;
using POS.ViewModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace POS.Repository
{
    [Table("tbl_products")]
    public class ProductEntity
    {
        [Key]
        [Column("product_id")]
        public int Id { get; set; }
        [Column("product_name")]
        public string Name { get; set; }
        [Column("supplier_id")]
        public int SupplierId { get; set; }

        public SupplierEntity Supplier { get; set; }

        [Column("category_id")]
        public int CategoryID { get; set; }

        public CategoryEntity Category { get; set; }

        [Column("quantity_per_unit")]
        public int Quantity { get; set; }
        [Column("unit_price")]
        public double Price { get; set; }
        [Column("unit_in_stock")]
        public int Stock { get; set; }
        [Column("unit_on_order")]
        public int Order { get; set; }
        [Column("reorder_level")]
        public int ReorderLevel { get; set; }
        [Column("discontinued")]
        public bool Discontinued { get; set; }

        public ICollection<OrderDetailEntity> OrderDetails { get; set; }

        public ProductEntity()
        {

        }
        public ProductEntity(ProductModel vmodel)
        {
            Name = vmodel.Name;
            SupplierId = vmodel.SupplierId;
            
            CategoryID = vmodel.CategoryID;
            
            Quantity = vmodel.Quantity;
            Price = vmodel.Price;
            Stock = vmodel.Stock;
            Order = vmodel.Order;
            ReorderLevel = vmodel.ReorderLevel;
            Discontinued = vmodel.Discontinued;
        }

    }
}